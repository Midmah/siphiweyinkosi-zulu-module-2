import 'dart:core';
import 'dart:io';
import 'dart:convert';

void main() {
  var mtnApps = new winningApp();
  mtnApps.appName = 'EasyEquity';
  mtnApps.sector = 'investment sector';
  mtnApps.developer = 'Charles Savage';
  mtnApps.year = 2020;

  print(mtnApps.convertToUpper("$mtnApps.appName"));
}

class winningApp {
  String? appName;
  String? sector;
  String? developer;
  int? year;

  String convertToUpper(String appName) {
    return appName.toUpperCase();
  }
}
