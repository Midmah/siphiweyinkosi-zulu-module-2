import 'dart:core';
import 'dart:io';

void main() {
  var winningApps = [
    "FNB Banking",
    "SnapScan",
    "Live Inspect",
    "WumDrop",
    "Domesty",
    "Standard Bank Shyft",
    "Khula",
    "Naked Insurance",
    "EasyEquities",
    "Amabani"
  ];

  var year = 2012;
  var num = 0;
  for (var wonApp in winningApps) {
    print("The winning app for $year is $wonApp");
    year++;
    num++;
  }

  for (var won in winningApps) {
    if (year == 2017 || year == 2018) {
      print("The winning app for 2017 $won, for 2018 $won");
    }
  }

  print("The total apps : $num");
}
